# CMoA - Cloud-native Magma on Arm
Cloud-native Magma on Arm containerizes Magma control plane and data plane, and provides cloud-native access gateway functions for wireless core network on Arm architectures.

## Description
CMoA runs the containerized AGW, which could be traffic-tested using S1AP or SRS-RAN tester. The testers can run on the same machine as other containers/VM or on different machines. There will be no changes to the underlying orchestration framework.
The CMoA architecture is shown in the following figure:
!["Cloud-native Magma on Arm"](doc/imgs/Cloud-native-Magma.png "Cloud-native Magma on Arm")

## Getting started
...

## Examples
...

## Key features
* Makes use of the K*S template
* Integrates Magma AGW control plane and data plane
* Supports inter-node and intra-node high availability and scalability
* Supports multi-tenancy
* Uses S1AP or SRS-RAN tester
* UE attach/detach procedures, call setup/tear down with traffic

## Use cases
* 3G/4G serving gateway
* 5G UPF - private 5G network

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License
With the exceptions recorded below, CMoA is distributed under the terms of the Apache License Version 2.0.
SPDX Identifier|TB Approval Date|GB Approval Date|File name
:--|:--:|:--:|--:
1.MIT|10/23/2019|02/10/2020|xxx/xxx/xxxxx.h
2.BSD-2-Clause|10/23/2019|12/18/2021|xxx/xxx/xxxxx.c

Some files in CMoA contain a different license statement. Those files are licensed under the license contained in the file itself.

CMoA also bundles patch files, which are applied to the sources of the various packages. Those patches are not covered by the license of CMoA. Instead, they are covered by the license of the software to which the patches are applied. When said software is available under multiple licenses, the CMoA patches are only provided under the publicly accessible licenses.

CMoA uses first line of the file to be SPDX tag. In case of *#!* scripts, SPDX tag can be placed in 2nd line of the file.

For example, to label a file as subject to the Apache-2.0 license, the following text would be used:
```
// SPDX-License-Identifier: Apache-2.0
```
or
```
#!/usr/bin/env bash
# SPDX-License-Identifier: Apache-2.0
```
